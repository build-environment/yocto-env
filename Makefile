DIR_PATH := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
DOCKER_REGISTRY := registry.gitlab.com/build-environment
DOCKER_IMAGE_NAME = $(DOCKER_REGISTRY)/yocto-env
PARENT_DOCKER_IMAGE = $(DOCKER_REGISTRY)/build-environment:latest

build:
	docker build $(DIR_PATH) --build-arg FROM=$(PARENT_DOCKER_IMAGE) --tag $(DOCKER_IMAGE_NAME)

deploy: build
	docker push $(DOCKER_IMAGE_NAME)

all: build

.PHONY : all build deploy
.DEFAULT_GOAL := all
