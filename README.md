# yocto-env

[![license](https://img.shields.io/gitlab/license/build-environment/yocto-env)](https://gitlab.com/build-environment/yocto-env)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

`yocto-env` is a docker image based on
[build-environment](https://gitlab.com/build-environment/build-environment)
which provides all tools to build a yocto project.

This `yocto-env` docker image contains all tools required by
[Yocto Project Quick Build](https://docs.yoctoproject.org/dev/brief-yoctoprojectqs/index.html#build-host-packages)
manual in addition with `git-repo`.

## How-to build the docker image

To build the image, use the Makefile:

```shell
$ make build
...
```

## How-to launch the docker image

The `yocto-env` docker image can be started using `yocto-env` script:

```shell
$ ./yocto-env [... "docker run additional arguments"] \
> [-c ... "command executed in container"]
...
```

## How-to contribute

To contribute to this project, please ensure that the following tools are installed:

* gitlint
* pre-commit
* shfmt
* shellcheck

You can automatically check and install the tools using the following command:

```shell
$ ./dev-env-setup.sh
Start of dev environment setup
Checking tools...
* pip... OK
* shfmt... OK
* shellcheck... Installed
* pre-commit... OK
* gitlint... OK

Checking hooks...
* pre-commit... Installed
* commit-msg... OK

Dev environment is ready!
```
