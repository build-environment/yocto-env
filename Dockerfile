ARG FROM=build-environment:latest

# ------------------------------------------------------------------------------
# Pull base image
FROM ${FROM}

# ------------------------------------------------------------------------------
# Install tools via apt
RUN export DEBIAN_FRONTEND=noninteractive \
    && set -x \
    && apt-get -y update \
    && apt-get -y install \
        gawk \
        wget \
        curl \
        file \
        git \
        diffstat \
        unzip \
        texinfo \
        gcc \
        build-essential \
        chrpath \
        socat \
        cpio \
        locales \
        python3 \
        python3-pip \
        python3-lxml \
        python3-pexpect \
        python-is-python3 \
        xz-utils \
        debianutils \
        iputils-ping \
        python3-git \
        python3-jinja2 \
        libegl1-mesa \
        libsdl1.2-dev \
        pylint \
        xterm \
        python3-subunit \
        mesa-common-dev \
        zstd \
        liblz4-tool \
        vim \
        nano \
        bash-completion \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists

# ------------------------------------------------------------------------------
# Install git-repo
# https://github.com/GerritCodeReview/git-repo
RUN set -x \
    && curl https://raw.githubusercontent.com/GerritCodeReview/git-repo/stable/repo > /usr/bin/repo \
    && curl https://raw.githubusercontent.com/GerritCodeReview/git-repo/stable/completion.bash > /etc/bash_completion.d/git-repo \
    && chmod +rx /usr/bin/repo

# ------------------------------------------------------------------------------
# Display, check and save environment settings
WORKDIR /root
COPY resources/.bashrc .

CMD [ "/bin/bash" ]
